const path = require('path');

module.exports = {
  mode: 'development', // или 'production'
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'djapp/static'),
    filename: 'bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
          },
        },
      },
      {
        test: /\.css$/,
        exclude: /node_modules/,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
  resolve: {
    fallback: {
      "path": require.resolve("path-browserify"),
      "fs": false,
      "crypto": false,
    },
    alias: {
      jquery: path.resolve(__dirname, 'node_modules/jquery/src/jquery'),
    },
  },
};

