// scripts.js
$(document).ready(function() {
  $('.like-form').on('submit', function(e) {
    e.preventDefault();
    var form = $(this);
    var url = form.data('url');

    $.ajax({
      type: 'POST',
      url: url,
      data: form.serialize(),
      dataType: 'json',
      success: function(response) {
        if (response.success) {
          var button = form.find('button');
          var likesCount = form.find('strong');

          if (response.is_liked) {
            button.removeClass('btn-success').addClass('btn-warning').text('Push to Unlike');
          } else {
            button.removeClass('btn-warning').addClass('btn-success').text('Push to Like');
          }

          likesCount.text(response.likes_count + ' likes');
        }
      },
      error: function(xhr, errmsg, err) {
        console.log(errmsg);
      }
    });
  });

  $('.follow-form').on('submit', function(e) {
    e.preventDefault();
    var form = $(this);
    var url = form.data('url');

    $.ajax({
      type: 'POST',
      url: url,
      data: form.serialize(),
      dataType: 'json',
      success: function() {
        var button = form.find('button');
        if (button.hasClass('btn-success')) {
          button.removeClass('btn-success').addClass('btn-warning').text('Push to unfollow');
        } else {
          button.removeClass('btn-warning').addClass('btn-success').text('Push to follow');
        }
      },
      error: function(xhr, errmsg, err) {
        console.log(errmsg);
      }
    });
  });
});
